# catID

[![pipeline status](https://gitlab.com/animalid/catid/badges/main/pipeline.svg)](https://gitlab.com/animalid/catid/-/pipelines) 
[![coverage report](https://gitlab.com/animalid/catid/badges/main/coverage.svg)](pages_link/htmlcov)
[![documentation](https://img.shields.io/badge/documentation-main-blue)](pages_link)

Detect your cat on raspberry pi from other cats.
